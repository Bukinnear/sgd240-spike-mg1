// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "ThirdPersonGameMode.generated.h"

/**
 * 
 */
UCLASS()
class THIRDPERSONBOMBERMAN_API AThirdPersonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AActor* ChoosePlayerStart_Implementation(AController* Player) override;


};
