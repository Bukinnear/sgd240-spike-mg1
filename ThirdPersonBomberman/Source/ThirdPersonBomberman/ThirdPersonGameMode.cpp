// Fill out your copyright notice in the Description page of Project Settings.

#include "ThirdPersonBomberman.h"
#include "ThirdPersonGameMode.h"
#include "EngineUtils.h"

AActor* AThirdPersonGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	// Empty PlayerStart object
	APlayerStart* FoundPlayerStart = nullptr;

	// Array to hold Character objects
	TArray<ACharacter*> CharacterArray;

	// Iterate through all of the ACharacter objects currently in the world
	for (TActorIterator<ACharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ACharacter* CharacterActor = *ActorItr;

		// If the object does not exist/is invalid, skip it.
		if (!CharacterActor)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerCharacter object is null"));
			continue;
		}
		if (!CharacterActor->IsValidLowLevel())
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerCharacter not valid, exiting"));
			continue;
		}

		// Add to array
		CharacterArray.Add(CharacterActor);

		// This might be useful for debugging.
		//Cast<APlayerController>(Player)->ClientMessage(ActorItr->GetName());
		//Cast<APlayerController>(Player)->ClientMessage(ActorItr->GetActorLocation().ToString());
	}

	// Array to hold PlayerStart objects
	TArray<AActor*> PlayerStartArray;

	// Iterate through all of the PlayerStart objects currently in the world & add them to array
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStartArray);

	// If there are other player characters in the world
	if (CharacterArray.Num() > 0)
	{
		float GreatestAverageDistance = 0.0f;
		AActor* GreatestAverageActor = nullptr;

		// Iterate through the PlayerStart actors
		for (int PlayerStartIndex = 0; PlayerStartIndex < PlayerStartArray.Num(); PlayerStartIndex++)
		{
			// The current PlayerStart object we are working with
			auto CurrentStartActor = PlayerStartArray[PlayerStartIndex];
			auto CurrentStartActorLocation = CurrentStartActor->GetActorLocation();
			auto CurrentStartActorRotation = CurrentStartActor->GetActorRotation();

			float CurrentAverageDistance = 0.0f;

			// Iterate through every character in the PlayerCharacter array.
			for (int CharacterIndex = 0; CharacterIndex < CharacterArray.Num(); CharacterIndex++)
			{
				// The current character we are working with
				auto CurrentCharacter = CharacterArray[CharacterIndex];
				auto CurrentCharacterLocation = CurrentCharacter->GetActorLocation();

				// check if spawning the character here would collide
				bool SpawnIsBlocked = GetWorld()->EncroachingBlockingGeometry(CurrentCharacter, CurrentStartActorLocation, CurrentStartActorRotation);

				// Get the distance between the current character and current PlayerStart actor
				float Distance = FVector::DistSquared(CurrentStartActorLocation, CurrentCharacterLocation);

				//UE_LOG(LogTemp, Warning, TEXT(" \nStart Index %i \nCharacter %i \nStartActor %s\n "), PlayerStartIndex + 1, CharacterIndex + 1, *CurrentStartActor->GetName());

				// If this spawn is occupied or blocked, skip it.
				if (Distance < 50 || SpawnIsBlocked) { break; }

				// Add the distance to our current tally
				CurrentAverageDistance += Distance;

				// if we are at the of the array
				if (CharacterIndex == CharacterArray.Num() - 1)
				{
					// find the average of our tally
					CurrentAverageDistance /= CharacterArray.Num();
					//UE_LOG(LogTemp, Warning, TEXT("Distance: %f\n "), CurrentAverageDistance);
				}

				// if the current spawn point's average distance to all players is greater than our current, save it
				if (CurrentAverageDistance > GreatestAverageDistance)
				{
					GreatestAverageDistance = CurrentAverageDistance;
					GreatestAverageActor = CurrentStartActor;
				}
			} // end of character loop
		} // end of playerstart loop
		
		// Take 
		if (GreatestAverageActor)
		{
			// Cast to PlayerStart
			FoundPlayerStart = Cast<APlayerStart>(GreatestAverageActor);
		}

		// If the cast was successful, return the found PlayerStart
		if (FoundPlayerStart)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Returning %s\n "), *FoundPlayerStart->GetName());
			return FoundPlayerStart;
		}
	}
	// Find the default Start position for player 1
	else
	{
		// Find the default object by tag
		TArray<AActor*> AllDefaultPlayerStart;
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName(TEXT("DefaultPlayerStart")), AllDefaultPlayerStart);

		// If we found multiple objects
		if (AllDefaultPlayerStart.Num() > 1)
		{
			UE_LOG(LogTemp, Warning, TEXT("Multiple DefaultPlayerStart objects found! Returning first one discovered."));
		}

		// Iterate through the found object/s for the first valid PlayerStart object & return it
		for (AActor* DefaultPlayerStart : AllDefaultPlayerStart)
		{
			if (DefaultPlayerStart)
			{
				if (DefaultPlayerStart->IsValidLowLevel())
				{
					FoundPlayerStart = Cast<APlayerStart>(DefaultPlayerStart);

					if (FoundPlayerStart)
					{
						return FoundPlayerStart;
					}
				}
			}
		}
	}

	// if we find no other option, choose randomly
	if (FoundPlayerStart == nullptr)
	{
		FoundPlayerStart = Cast<APlayerStart>(PlayerStartArray[FMath::RandRange(0, PlayerStartArray.Num() - 1)]);
		//UE_LOG(LogTemp, Warning, TEXT("Could not find a valid placement"));
	}
	// Return PlayerStart
	return FoundPlayerStart;
}