---
title: Spike Report
---

MG 1 – Replication
==================

Introduction
------------

We want to learn how to make multiplayer games within Unreal Engine, and
what the pros and cons are of the UE4 method compared to other options
available.

Goals
-----

1.  Answer:

    a.  Tech: How does Unreal Engine do multiplayer?

    b.  Knowledge: What multiplayer architectures are available for use,
        and what does UE4 use?

    c.  Knowledge: What is replication, what types exist, and how do we
        use it?

    d.  Knowledge: What is a remote procedure call (RPC)? What does a
        Client “own” in UE4?

    e.  Skill: Setting up basic multiplayer game in Unreal Engine, using
        blueprints.

Create a small multiplayer game based on the Third-Person (blueprint)
template:

1.  Be able to load up to 4 players via the editor Multiplayer Play
    option.

    a.  Each should start with their own Character at one of several
        Player Start actors

    b.  You should select the Player Start actor to use based on some
        metric, like the furthest away from any existing players.

    c.  Player movement and shooting should be properly replicated.

2.  You must add the following two features:

    a.  The ability to throw a bomb and damage people

    b.  A coin/pickup system

        i.  A number of coins are spawned around the level (perhaps
            randomly)

        ii. Each player’s coin total is visible to all players

            1.  Consider how this information is stored – using the
                Unreal Engine Gameplay Framework (spike 4). Would this
                be part of the GameState or PlayerState classes – rather
                than a specific Pawn or Controller?

            2.  Use a simple UI widget to show this

            3.  You could use RepNotify to force update the UI\
                (rather than updating every frame)

        iii. Coins, as they get picked up, play a particle
            effect/animation

            1.  Only for nearby players

        iv. Coins which have been picked up are removed from the world

            1.  For all players – so you never see a coin that doesn’t
                exist

Throughout this, you must use all of the following (and document
examples):

1.  Actor replication

2.  Variable Replication

3.  Variable Replication with RepNotify

4.  Function Calls (RPCs)

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

Provide resource information for team members who wish to learn
additional information from this spike.

-   Introduction to UE4 Networking:
    <https://www.youtube.com/playlist?list=PL56pcT0mYsOgFmbwukjKppPI_Q7E89jTK>

-   Finding Actors in a level:
    <https://answers.unrealengine.com/questions/289453/get-all-actors-of-class-in-c.html>

-   Object Iterators (and finding actors/objects in a level):
    <https://wiki.unrealengine.com/Iterators:_Object_%26_Actor_Iterators,_Optional_Class_Scope_For_Faster_Search>

-   GamePlayStatics (Finding objects based on class/tag):
    <https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Kismet/UGameplayStatics/index.html>

-   Dynamic Arrays: <https://wiki.unrealengine.com/Dynamic_Arrays>

-   Cast&lt;DesiredObjectType&gt;(SourceVariable);

-   FVector::DistSquared() :
    <https://docs.unrealengine.com/latest/INT/API/Runtime/Core/Math/FVector/DistSquared/index.html>

-   Rotate object to always face player
    <https://answers.unrealengine.com/questions/40413/3d-object-always-facing-playercamera.html>

-   Netcode 101 (basic network concepts):
    <https://www.youtube.com/watch?v=hiHP0N-jMx8&feature=youtu.be>

-   Actors and Their Owning Connections:
    <https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Actors/OwningConnections/index.html>

Tasks undertaken
----------------

### Multiple players and Spawning

1.  Within the editor, set the number of players by clicking the arrow
    next to the play button -&gt; Number of Players.

2.  To accommodate this, ensure that the scene contains 1 or more
    PlayerStart objects, and that the Default Pawn Class is properly set
    in the GameMode settings. The game will automatically spawn &
    possess pawns for each player.

3.  By default, characters & pawn classes will replicate, but the option
    can be found in the class’ blueprint defaults.

4.  To select a spawn position based on criteria, it is easiest
    accomplished by overriding the ChoosePlayerSpawn() function that is
    automatically called in the GameMode (See the [Overriding Notes in
    the What We Found Out section](#overriding-notes) for how to
    accomplish this).

5.  In this project, we used C++ to spawn the character at the
    PlayerSpawn object with the greatest average distance from all other
    existing characters, although this would probably be easier
    accomplished in blueprint. The steps to achieve this follow, and
    refer to the [Overriding Notes in the What We Found Out
    section](#overriding-notes):

    a.  Get all PlayerCharacter objects (**NOTES:** [Step
        4](#overriding-notes))

    b.  If there are no player character, get the default PlayerStart
        actor by tag (**NOTES:** [Step 4](#overriding-notes)) (add the
        tag in the editor)

    c.  If there is more than 1 PlayerCharacter, get all PlayerStart
        actors

    d.  Set 2 variables

        i.  GreatestAverageDistance

        ii. GreatestAverageActor

    e.  For each PlayerStart actor:

        i.  Make a CurrentAverageDistance variable

        ii. For each PlayerCharacter actor:

            1.  Get the [distance
                squared](https://docs.unrealengine.com/latest/INT/API/Runtime/Core/Math/FVector/DistSquared/index.html)
                to each PlayerCharacter

            2.  If the distance is too close to the player, or spawning
                is blocked (**NOTES:** [Step 5](#overriding-notes)),
                skip this spawn position.

            3.  Add the distance to CurrentAverageDistance

            4.  At the end of the loop, divide CurrentAverageDistance by
                the number of elements in the array to find the average
                distance

            5.  If the CurrentAverageDistance is greater than the
                GreatestAverageDistance, set it to be equal to the
                current average, and set the GreatestAverageActor to be
                equal to the current PlayerStart actor.

    f.  Return the GreatestAverageActor

    g.  If there is no valid PlayerStart actor returned after the above,
        get all PlayerStart actors, and select one randomly as a safety
        precaution.

### Throwing Bombs

This section is based on the Introduction to Networking videos linked in
the [Technologies, Tools, and Resources
section](#technologies-tools-and-resources-used), episodes 5 & 6 in
particular.

Bomb Class:

1.  Create a new class for your bomb.

2.  Add a Projectile Movement called ProjectileMovement, and a Static
    Mesh component called BombMesh.

3.  Add the following variables:

  Name                          Type                        Replication   Default Value
  ----------------------------- --------------------------- ------------- -----------------------------------------
  BombMID                       Material Instance Dynamic   None          None
  Armed                         Boolean                     RepNotify     False
  MaterialColourParameterName   Name                        None          Color
  FuseTime                      Float                       None          3.0
  ExplosionRadius               Float                       None          200.0
  ExplosionDamage               Float                       None          25.0
  ExplosionEffects              ParticleSystem              None          P\_Explosion (Or your preferred effect)
                                                                          

1.  Create a custom event in the graph called OnExplosion

2.  Add the following functions:

    ![](media/image1.png){width="3.0520833333333335in"
    height="1.40625in"}

Constructor – Create Dynamic Material Instance with the BombMesh as the
target, and set BombMID to be equal to the result. Call Set Bomb Color

OnRep\_Armed – Call SetBombColor if Armed = True

SetBombColor – Takes a Linear Color Structur – Sets the BombMID’s
parameter with the name contained by MaterialColorParamName to be equal
to the provided color.

OnFuseExpired – Continue only on server (**NOTES:** See step 1 in the
[Bomb Throwing Notes](#bomb-throwing-notes) for how to do this) –
trigger Apply Explosion Damage, trigger On\_Explosion, call Set Life
Span.

ApplyExplosionDamage – Applies damage equal to ExplosionDamage to all
objects of the pawn type within the range of ExposionRadius.

ThirdPersonCharacter (or your pawn class):

1.  Add a Text Render Component called CharText, and position it above
    the character

2.  Add the following variables - replication (**NOTES:** See [What is
    Replication in the Knowledge
    section](#knowledge-what-is-replication-what-types-exist-and-how-do-we-use-it)):

  Name                Type    Replication   Default Value
  ------------------- ------- ------------- ----------------------------
  Health              Float   RepNotifiy    0.0
  BombCount           Int     RepNotify     0
  MaxBombCount        Int     None          3
  MaxHealth           Float   None          100.0
  BombClass           Class   None          \[Your Custom Bomb Class\]
  BombSpawnDistance   Float   None          200.0
                                            

1.  Ensure your pawn class contains these functions:

    ![](media/image2.png){width="3.0208333333333335in"
    height="2.3958333333333335in"}

There are quite a few functions for this section, but most of them are
quite simple. Their roles are as follows:

InitAttributes – Call InitHealth & InitBombs

InitHealth – Set Health to MaxHealth

InitBombs – Set Bombs to MaxBombs

OnRep\_Health – Call Update CharacterDisplayText

OnRep\_BombCount – Call Update CharacterDisplayText

UpdateCharacterDisplayText – Using BuildString, set the text of the
CharText to equal to

“Health: \[Health Value\] Bomb Count: \[BombCount Value\]”

OnTakeDamage – Takes a float value called Damage - Using an Authority
Switch to continue only on the server, set health to be equal to health
– damage. If health is &lt;= 0, call InitHealth

HasBombs – Returns a Boolean, If BombCount &gt; 0

AttemptSpawnBomb – Continue only on server – Spawn a BombClass actor in
front of our character, and decrement BombCount by 1.

### Collectible Coins

See the [What We Found Out section](#_Collectibles) for general notes
about this process.

1.  Start by creating a coin object – this project simply used a rotated
    cylinder with gold material.

2.  Add an integer variable called Score

3.  For aesthetics, the coin sets a random rotation on spawn, and
    rotates a set amount every tick.

4.  Allow the actor to replicate:

    ![](media/image3.png){width="3.7125984251968505in"
    height="2.1811023622047245in"}

5.  Create a custom PlayerState class.

6.  Create a function in the player pawn, and PlayerState class called
    AddScore that takes an interger called Score

7.  In the PlayerState class, add a replicated int variable called
    CollectibleScore

    ![](media/image4.png){width="2.4488188976377954in"
    height="1.2519685039370079in"}

8.  On ActorBeginOverlap:

    a.  Check if the colliding actor is a player, if yes:

        i.  Trigger a customer event; OnCollected

        ii. Use an authority switch to continue only on the server:

        iii. Cast to the player pawn class, and call AddScore, passing
            our score variable

        iv. Destroy this actor

9.  OnCollected:

    a.  Spawn an Emitter at this actor’s location

    b.  For the emitter, set the Max Draw distance (this project used a
        distance of 1000)

10. PlayerPawn::AddScore:

    a.  Get the PlayerState, cast it to your custom class, and call
        AddScore, passing the score int we received

11. PlayerState::AddScore

    a.  Use an authority switch to have the server add the given score
        to CollectibleScore

12. Place the coins in the world - this project uses the game mode to
    spawn coins on TargetPoint actors with the CollectibleTarget tag.

### Score UI

This was a troublesome and inefficient component to create, see Open
Issues and Risks and Recommendations if you are planning to reproduce
this in a different project.

Setup:

1.  In the PlayerState, add 2 int variables – PlayerNumber and
    NumPlayers (you may want to choose a less confusing set of names)

    a.  Set PlayerNumber to Replicated, and NumPlayers to ReNotify

        ![](media/image5.png){width="2.622047244094488in"
        height="1.188976377952756in"}

2.  Create a custom GameMode, and PlayerController class

3.  In the GameMode, add a new int variable, NewPlayerNumber

GameMode:

1.  Using the OnPostLogin event, have the server get the PlayerState
    from the provided PlayerController

2.  Increment the value of NewPlayerNumber

3.  Set the PlayerState’s PlayerNumber value to be equal to
    NewPlayerNumber

4.  After this, use GetPlayerArray from GetGameState

5.  For each PlayerState, use GetNumPlayers to set the PlayerState’s
    NumPlayers to be equal to that

ScoreWidget:

1.  Create a new UI element called ScoreWidget

2.  Add 2 new TextBlocks

    a.  The first one simple displays the word “Player”

    b.  The second called IndexScoreText will hold the player’s number
        and score – this needs to be made a variable

3.  Add a PlayerState variable

4.  In the graph, have the IndexScoreText updated based on the
    PlayerState’s PlayerNumber and CollectibleScore variables every tick
    (See the ScoreWidget file for more details)

5.  See the ScoreWidget class file for how this should look

PlayerController:

1.  Add 2 variables, a 2d vector called WidgetScreen Offset and a int
    array called TrackedPlayers

2.  Create a new function called AddPlayerScoreUI

    a.  This accepts a PlayerState, creates a new ScoreWidget, positions
        it on screen by multiplying the offset variable by the
        PlayerState’s player number, and adds it to the viewport.

3.  Create a new function called UpdateNumPlayers

    a.  Loop through all PlayerState classes in the scene, for each,
        check if the player number is contained by the TrackedPlayers
        array – if it is not, add it to the array, and call the
        AddPlayerScoreUI function.

PlayerState:

1.  In the OnRep\_NumPlayers function, cast the local player controller
    (**NOTES:** Step 2 of the [Collectible UI
    Notes](#collectibles-ui-notes)) t0 your custom class, and call
    UpdateNumPlayers.

What we found out
-----------------

### Process

This section will contain the notes made in the process of the [Tasks
Undertaken section](#tasks-undertaken)

#### Overriding Notes

1.  To override a function in blueprint, open the class file and click
    Override in the functions section and choose the function you wish
    to override.

2.  To do the same in C++, you must declare the function you intend to
    override in the header, in this case it looks like:

> public:
>
> AActor\* ChoosePlayerStart\_Implementation(AController\* Player)
> override;

a.  Note the \_Implementation in the name – this is due to the function
    being a BlueprintNativeEvent and as such, cannot be overridden
    directly via C++

<!-- -->

1.  To get actors in C++, you can either use:

    a.  UGameplayStatics (GetAllActorsOfClass / WithTag)

        i.  Easy to use, requires an array to save results to

        ii. Array must be an AActor\* array

    b.  Actor Iterator.

        i.  Requires more setup and is more verbose, but also more
            flexible than GamePlayStatics, as you can add logic in the
            search loop to refine your search parameters

        ii. See the [Technologies, Tools and Resources Used
            section](#technologies-tools-and-resources-used) for details

2.  To check if a spawn point is blocked, use

> GetWorld()-&gt;EncroachingBlockingGeometry(ActorToFit, ActorLocation,
> ActorRotation);

a.  This does not return true if there is an actor in the general
    vicinity, only if it is directly on the spawn point.

#### Bomb Throwing Notes

1.  There are 2 ways to perform an action exclusively either on the
    server, or on client machines

    a.  Use an Authority Switch:

        ![](media/image6.png){width="1.53125in"
        height="0.8541666666666666in"}

    b.  Use a [replicated Remote Procedure
        Call](#knowledge-what-is-replication-what-types-exist-and-how-do-we-use-it)

[]{#_Collectibles .anchor}

#### Collectibles Notes

1.  You cannot get other player’s controllers on client machines, if you
    want to work with them, it needs to be done on the server.

2.  You *can* get other player’s PlayerStates, but, again, unless you
    are on the server, you can’t find which controller/player they came
    from. More on this in the **ScoreUI** section.

3.  The PlayerState is attached, and can be accessed, through the
    PlayerController

4.  Character actors have a direct reference to the PlayerState without
    first getting the PlayerController

#### Collectibles UI Notes

1.  If you update all of your scores via PlayerState, there will be a
    certain degree of delay between a client touching a collectible, and
    the score being displayed on their screen.

2.  The local PlayerController always has the index of 0

### Knowledge

This section of the report seeks to fill our knowledge gap regarding
Unreal Editor 4’s multiplayer capabilities, and how to use them.

####### **Tech: How does Unreal Engine do multiplayer?**

Taken from the [Networking
Overview](https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Overview/index.html)
of the UE4 docs:

*“UE4 networking is built around the server/client model. This means
that there will be one server that is authoritative (makes all the
important decisions), and this server will then make sure all connected
clients are continually updated so that they maintain the most up to
date approximation of the server's world. Even non-networked,
single-player games have a server; the local machine acts as the server
in these cases.”*

####### **Knowledge: What multiplayer architectures are available for use, and what does UE4 use?**

UE4 uses the client/server networking model. This can be either as
dedicated server, or a client hosted setup.

Other models include peer-to-peer and mesh models, as well as hybrid
combinations between them all. A more in depth guide to networking
models and their details can be found
[here](https://www.youtube.com/watch?v=hiHP0N-jMx8&feature=youtu.be)

####### **Knowledge: What is replication, what types exist, and how do we use it?**

Replication is the process of synchronising objects between connected
clients within a game. The server ensures that an object on client 1’s
machine exists, looks, and acts the same as the same object on all other
client machine. Depending on how replication is used, certain attributes
can be replicated, or not replicated across machines, depending on the
desired effect.

UE4 has several types of replication. For variables:

1.  Not replicated – no replication at all. The object can be completely
    different across machines

2.  Replicated – the object will be synchronised between machines

3.  OnRep\_Notified – Same as replicated, but triggers a function when
    the value is changed

And for Remote Proceedure Calls:

1.  None

2.  Runs on Server – only runs on the server

3.  Runs on Client – runs on all client machines, but not the server

4.  Multicast – runs on server and all clients

**\
**

**Knowledge: What is a remote procedure call (RPC)? What does a Client
“own” in UE4?**

A Remote Procedure Call, or RPC, is a function that is executed remotely
– meaning it runs on a different machine from the one that called it.

Regarding what a client “owns, the following is taken from the UE4 Docs
regarding [Actors and their Owning
Connections](https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Actors/OwningConnections/index.html):

*“Ultimately, each connection has a PlayerController, created
specifically for that connection (read more about this process in the
client connection flow details). Each PlayerController that is created
for this reason, is owned by that connection. To determine if an actor
in general is owned by a connection, you query for the actors most outer
owner, and if the owner is a PlayerController, then that actor is also
owned by the same connection that owns the PlayerController.*

*One example of this is when Pawn actors are possessed by a
PlayerController. Their owner will be the PlayerController that they are
possessed by. During this time, they are owned by the connection of the
PlayerController. The Pawn is only owned by this connection during the
time it is also owned/possessed by the PlayerController. So as soon as
the PlayerController no longer possesses the Pawn, the Pawn is no longer
owned by the connection“*

### Examples

####### Actor replication

![](media/image3.png){width="3.7125984251968505in"
height="2.1811023622047245in"}

1.  ThirdPersonCharacter

2.  Bomb\_BP

####### Variable Replication

![](media/image4.png){width="2.4488188976377954in"
height="1.2519685039370079in"}

1.  Collectible – ScoreValue

2.  ThirdPersonPlayerState\_BP - CollectibleScore

####### Variable Replication with RepNotify

1.  ThirdPersonPlayerState\_BP - NumPlayers

2.  Bomb\_BP - Armed

####### Function Calls (RPCs)

1.  ThirdPersonCharacter – ServerAttemptSpawnBomb

2.  Bomb\_BP - OnExplosion

 \[Optional\] Open Issues/risks
-------------------------------

-   You cannot override a BlueprintNativeEvent in C++, you must instead
    override FunctionName\_Implementation

\[Optional\] Recommendations
----------------------------

-   Make the (local) UI component independent from the PlayerState

You may state that another spike is required to resolve new issues
identified (or) indicate that this spike has increased the team’s
confidence in XYZ and move on.
